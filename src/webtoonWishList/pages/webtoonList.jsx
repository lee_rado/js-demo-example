import React, {useEffect, useState} from "react";

const WebtoonList = () => {
    // 웹툰 목록 관리
    const [ webtoons, setWebtoons ] = useState( [] );
    // 찜한 목록 관리
    const [ favorites, setFavorites ] = useState( JSON.parse(localStorage.getItem('favorites')) || [] );

    // 쿠키, 로컬스토리지 --> 처음에 App.js <- 항상 처음 시작 이 때 메모리에 접근해서 두곳에 있는 데이터 동기화 일단 한번 해주고
    // 연결해놓은 값 수정. 이 패키지가 알아서 쿠키, 로컬스토리지 값 변경
    // spa는 새로고침하면 데이터 날아감 왜?
    // v-dom ram에 임시로 올라가는 데이터
    // 새로고침 하면 v-dom이 가지고있는 데이터 날아간다.
    // 패키지 추가로 새로고침했을 때 데이터 안날라가게 + 주기적 동기화 : 쿠키, 로컬스토리지 한박자씩 느려. todo리스트 만드는 강의 바바. 한박자 처음에 다 느려. 새로고침 해야 나옴.
    // 동기식.

    // 1. 생성
    useEffect(() => {
        // public의 webtoons.json을 불러온다
        fetch('http://localhost:3000/webtoonWishList/webtoons.json')
            // 성공할 경우, 응답 결과를 json으로
            .then( response => response.json() )
            // 위가 성공할 경우, setWebtoons의 데이터에 넣는다.
            .then( data => setWebtoons( data ) )
    }, []);

    // 2. 업데이트
    useEffect(() => {
        //setItem : key와 value를 기반으로 저장한다. localStorage.setItem(key, value)
        // value 자리의 JSON.stringify(favorites)의 뜻은? 직렬화 된 ..
        localStorage.setItem('favorites', JSON.stringify(favorites))
    }, [favorites]);

    const addFavorite = (webtoon) => {
        // {"id": 1, "webtoonName": "웹툰1", "webtoonDescription": "웹툰1에 대한 설명"}
        if (favorites.find(v => v.id === webtoon.id)) {
            setFavorites(favorites.filter(item => item.id !== webtoon.id));
        } else {
            setFavorites([...favorites, webtoon]);
        }
    }

    return (
        <div>
            <h1>웹툰 리스트</h1>
            {webtoons.map(item => (
                <div key={item.id}>
                    <h3>{item.webtoonName}</h3>
                    <p>{item.webtoonDescription}</p>
                    <button onClick={() => addFavorite(item)}>
                        {favorites.find(v => v.id === item.id) ? '🧡' : '♡'}
                    </button>
                </div>
            ))}
        </div>

    )
}

export default WebtoonList