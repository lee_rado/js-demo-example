import React from "react";

const FavoriteList = () => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];

    return (
        <div>
            <h1>Favorite List</h1>
            {favorites.map((item,index) => (
                <div key={item.id}>
                    <p>{item.webtoonName}</p>
                </div>
            ))}
        </div>
    )
}
export default FavoriteList