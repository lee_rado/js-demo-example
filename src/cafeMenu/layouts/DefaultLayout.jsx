import React from "react";
import {Link} from "react-router-dom";

const DefaultLayout = ({ children }) => {
    return (
            <>
                <div>
                    <div className="App-header">
                        <h3>카페 붐붐</h3>
                    </div>
                    {/*<nav>*/}
                    {/*    <Link to="/">①</Link>*/}
                    {/*    <Link to="/detail">②</Link>*/}
                    {/*</nav>*/}
                </div>
                <main>{children}</main>
                <footer><p>붐붐붐 푸터입니다</p></footer>
            </>
        )
}

export default DefaultLayout;