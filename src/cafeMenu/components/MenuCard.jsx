import React from 'react';

const MenuCard = ({ menu, addToCart }) => (
    <div onClick={() => addToCart(menu)}>
        <img src={process.env.PUBLIC_URL + '/cafeMenu/assets/' + menu.img} alt="coffee" width={280}/>
        <h3>{menu.name}</h3>
        <p>{menu.price}원</p>
    </div>
)
export default MenuCard;