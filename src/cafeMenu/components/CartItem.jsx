import React from 'react';

const CartItem = ({ item, removeFromCart }) => (
        <div>
            <span>{item.name} / 수량 : {item.quantity} / 가격 : {item.price}</span>
            <button onClick={() => removeFromCart(item)}>삭제</button>
        </div>
    )

export default CartItem;