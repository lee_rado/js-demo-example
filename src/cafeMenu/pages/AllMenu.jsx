import React, {useCallback, useEffect, useState} from "react";
import MenuCard from "../components/MenuCard";
import CartItem from "../components/CartItem";

const AllMenu = () => {
    // 메뉴 항목을 저장할 상태 변수
    const [menu, setMenu] = useState([]);
    // 장바구니 항목을 저장할 상태 변수
    const [cart, setCart] = useState([]);

    // 컴포넌트가 마운트될 때 메뉴 데이터를 가져옴
    useEffect(() => {
        fetch('http://localhost:3000/cafeMenu/menu.json')
            .then(res => res.json())
            .then( data => setMenu(data))
    }, []);

    // 항목을 장바구니에 추가하는 함수
    // useCallback을 사용해서 불필요한 리렌더링을 방지한다.
    const addToCart = useCallback( menu => {
        setCart((prevCart) => {
            // 장바구니에 이미 있는 항목인지 확인
            const existingItem = prevCart.find((item) => item.name === menu.name)
            if (existingItem) {
                // 이미 있는 항목이라면 수량을 증가
                return prevCart.map((item) =>
                    // 현재 반복중인 장바구니 항목의 이름이 추가하려는 메뉴의 이름과 같은지 확인
                    item.name === menu.name
                        // 같다면 해당 항목의 수량을 1 증가시킨 새 객체 반환
                        ? { ...item, quantity: item.quantity + 1 }
                        // 다르다면 변경없이 원래의 항목을 반환
                        // => 이 경우는 map의 조건이다. 배열을 순회하면서 각 객체의 값을 비교하게 되는데,
                        // 이 과정에서 배열의 값에 속해있지 않은 다른 객체를 위한 조건이다.
                        : item);
            } else {
                // 없는 항목이라면 수량 1로 추가
                return [...prevCart, {...menu, quantity: 1}]
            }
        })
    },[])

    // 장바구니에서 항목을 제거하는 함수
    const removeFromCart = useCallback((menu) => {
        setCart((prevCart) => {
            // reduce 메서드로 장바구니 항목 처리
            return prevCart.reduce((acc, item) => {
                if (item.name === menu.name) {
                    // 항목의 수량이 1보다 많다면 수량 감소
                    if (item.quantity > 1) {
                        return [...acc, {...item, quantity: item.quantity - 1 }];
                    }
                } else {
                    // 제거할 항목이 아니면 그대로 유지
                    return [...acc, item];
                }
                return acc;
            },[])
        })
    }, [])


    // 장바구니 항목들의 총 금액을 계산하는 함수
    const calculateTotal = useCallback(() => {
        return cart.reduce((total, item) => total + item.price * item.quantity, 0);
    }, [cart])

    // 장바구니 항목들의 총 수량을 계산하는 함수
    const calculateTotalCount = useCallback(() => {
        return cart.reduce((total, item) => total + item.quantity, 0);
    }, [cart])


    return (
        <div>
            <div>
                <p className='category-header'>전체 메뉴</p>
                <div className="coffee-container">
                    {menu.map((menu) => (
                        <MenuCard key={menu.id} menu={menu} addToCart={addToCart}/>
                    ))}
                </div>
            </div>
            <div>
                <p className='category-header'>장바구니</p>
                <div className="order-container">
                    {cart.map((item) => (
                        <CartItem key={item.name} item={item} removeFromCart={removeFromCart}/>
                    ))}
                </div>
            </div>
            <div>총 수량 {calculateTotalCount()}잔 </div>
            <div className='category-header'> 총 금액 : {calculateTotal()}원 </div>
        </div>
    )
}
export default AllMenu