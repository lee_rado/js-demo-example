import {useRef, useState} from "react";

const ConnectGame = () => {
    const [word, setWord] = useState('');
    const [stack, setStack] = useState([]);
    const [result, setResult] = useState('');
    let inputRef = useRef(null);

    const handleChange = (e) => {
        setWord(e.target.value)
    }

    const onSubmit = (e) => {
        e.preventDefault()

        // 제약사항 생각하기
        // 동일한 단어 안됨
        // 이전의 단어 마지막 글자와 입력한 단어의 첫번째 글자가 일치해야한다.
        // 20개가 넘으면 안된다.
        // 만약 단어모음집의 길이가 0보다 크고 단어모음집의 마지막 글자와 입력한 글자가 같으면
        if(stack.length > 0 && stack[stack.length - 1].word === word) {
            setResult('동일한 단어 입력 안돼용')
            // 만약 모음집의 길이가 0 (=> 처음 입력하는 것) 이거나
        } else if (stack.length === 0 || stack[stack.length - 1].word.slice(-1) === word) {
            // 만약 단어모음집이 20개 미만이라면
            if( stack.length < 20) {
                setStack([...stack, { word, id: stack.length + 1 }])
                setWord('')
            } else {
                setResult('게임 끝!')
            }
        } else {
            setResult('단어의 첫 글자와 이전의 마지막 글자가 일치해야합니다.')
        }


    }

    return(
        <div>
            <h1>끝말잇기를 해보자 쿵쿵따</h1>
            <h3>{word}</h3>
            <form onSubmit={onSubmit}>
                <input ref={inputRef} type="text" onChange={handleChange} value={word}/>
                <button type="submit">확인</button>
            </form>
            <h3>{result}</h3>
            {stack.map((item, index) => (
                <p key={index}>
                    {item.id}. {item.word}
                </p>
            ))}
        </div>
    )

}
export default ConnectGame;