import {useState} from "react";

const ConnectGame2 = () => {
    const [word, setWord] = useState('')
    const [stack, setStack] = useState([])
    const [gameOver, setGameOver] = useState(false)

    const handleChange = (e) => {
        setWord(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if (stack.length > 0 && stack[stack.length - 1].word === word) {
            alert('마지막으로 제시된 단어와 동일한 단어를 입력할 수 없습니다.')
        } else if (stack.length === 0 || stack[stack.length - 1].word.slice(-1) === word[0]) {
            if (stack.length < 20) {
                setStack([...stack, { word, id: stack.length + 1 }])
                setWord('')
            } else {
                setGameOver(true)
            }
        } else {
            alert('단어의 첫 글자가 이전 단어의 마지막 글자와 일치해야 합니다.')
        }
    }

    const handleRestart = () => {
        setWord('')
        setStack([])
        setGameOver(false)
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input value={word} onChange={handleChange} disabled={gameOver} />
                <button type="submit" disabled={gameOver}>제출</button>
            </form>
            {stack.map((item, index) => (
                <p key={index}>
                    {item.id}. {item.word}
                </p>
            ))}
            {gameOver && <button onClick={handleRestart}>다시하기</button>}
        </div>
    )
}

export default ConnectGame2