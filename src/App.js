import './App.css';
import {Route, Routes} from "react-router-dom";
import DefaultLayout from "./cafeMenu/layouts/DefaultLayout";
import AllMenu from "./cafeMenu/pages/AllMenu";
import DetailMenu from "./cafeMenu/pages/DetailMenu";

function App() {
  return (
    <div className="App">
        <Routes>
            <Route path="/" element={<DefaultLayout><AllMenu /></DefaultLayout>} />
            <Route path="/detail" element={<DefaultLayout><DetailMenu /></DefaultLayout>} />
        </Routes>
    </div>
  );
}

export default App;
