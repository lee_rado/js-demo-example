import React, {useState} from 'react';

const TestRequest = () => {
    const [currentQuestion, setCurrentQuestion] = useState(0)
    const [score, setScore] = useState(0)


    // 목업데이터 (질문과 선택지)
    const questions = [
        {
            question: '1. 변의 색깔은 어떠셨나요?',
            answer: [
                { text: '황갈색', score: 10 },
                { text: '붉은색', score: 0 },
                { text: '검정색', score: 0 },
                { text: '회백색', score: 0 },
                { text: '초록색', score: 5 },
                { text: '노란색', score: 5 },
            ]
        },
        {
            question: '2.변의 형태는 어떠셨나요?',
            answer: [
                { text: '바나나 모양', score: 10 },
                { text: '가늘고 긴 모양', score: 5 },
                { text: '묽은 상태', score: 5 },
                { text: '자잘한 모양', score: 5 },
                { text: '끊긴 모양', score: 5 },
            ]
        },
        {
            question: '3.변을 보는 시간은 어떻게 되나요?',
            answer: [
                { text: '5분 이내', score: 10 },
                { text: '10분 이내', score: 5 },
                { text: '15분 이내', score: 0 },
            ]
        },
        {
            question: '4.변을 보는 빈도는 어떻게 되나요?',
            answer: [
                { text: '하루에 1~3번', score: 10 },
                { text: '2~3일에 한 번', score: 5 },
                { text: '4일에 한 번', score: 5 },
                { text: '일주일에 한 번', score: 5 },
                { text: '하루에 4번 이상', score: 0 },
            ]
        },
        {
            question: '5.변을 볼 때 통증이나 불편함을 느끼시나요?',
            answer: [
                { text: '없음', score: 10 },
                { text: '간혹 느낌', score: 5 },
                { text: '항상 느낌', score: 0 },
            ]
        },
        {
            question: '6.일주일 평균 운동량은 어떻게 되시나요?',
            answer: [
                { text: '4회 이상', score: 10 },
                { text: '1~4회', score: 5 },
                { text: '0회', score: 0 },
            ]
        },
        {
            question: '7.특정 약물을 복용하고 계신가요?',
            answer: [
                { text: '먹고 있다.', score: 0 },
                { text: '먹지 않는다.', score: 10 },
            ]
        },
    ]

    // 결과 함수
    // 먄약 70점(만점)이면 / 50점 이상이면 / 50점 미만이면으로 나눠서 결과값 관리
    const getResult = score => {
        if (score >= 70) {
            return '당신은 장이 매우 건강합니다. 이대로 유지해주세요.';
        } else if (score >= 50) {
            return '식습관 관리 및 주의가 필요합니다. 추후 관리를 위해 내원해주세요.';
        } else {
            return '장 건강을 위해 병원으로 내원해주세요.'
        }
    }

    // 핸들러
    const handleSubmit = answerScore => {
        // 점수 카운트 기존 점수 + 현재 답변한 선택지 점수
        setScore(score + answerScore)
        // 다음 질문은 현재 질문 + 1이다.
        const nextQuestion = currentQuestion + 1
        // 만약 다음 질문이 질문의 길이 보다 작을 경우 = 질문이 끝나지 않은 경우
        if (nextQuestion < questions.length) {
            // 질문 초기화 => 다음 질문으로
            setCurrentQuestion(nextQuestion)
        } else {
            // 만약 아니라면
            // getResult 결과 나오게
            alert(getResult(score + answerScore))
        }
    }


    return <div>
            {/* 질문들 목업의 현재 질문 인덱스의 질문*/}
            <h2>{questions[currentQuestion].question}</h2>
            {/*질문들 목업의 현재 질문 인덱스의 선택지*/}
            {/*answer을 누르면 핸들서밋으로 answer.score 점수를 보냄*/}
            {questions[currentQuestion].answer.map(answer => (
                <button key={answer.text} onClick={() => handleSubmit(answer.score)}>
                    {answer.text}
                </button>
            ))}
        </div>
}
export default TestRequest;